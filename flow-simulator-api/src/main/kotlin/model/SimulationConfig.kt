package model


data class FetchConfig(
    val totalItems: Int = 50,
    val itemsPerPage: Int = 10,
    val fetchTimeRange: LongRange
)

data class ProcessConfig(
    val concurrency: Int = 1,
    val processTimeRange: LongRange
)

data class SimulationConfig(
    val fetchConfig: FetchConfig,
    val bufferSize: Int = 0,
    val processConfig: ProcessConfig
)

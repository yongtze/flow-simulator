package model

data class Item(
    val itemId: Int
) {
    val entityType: String = "Item"
}

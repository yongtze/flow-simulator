package model

data class Page<ITEM>(
    val index: Int,
    val items: List<ITEM> = listOf(),
    val hasNext: Boolean = false
) {
    val entityType: String = "Page"
}

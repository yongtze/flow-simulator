package model


data class LaunchSimulationRequest(
    val requestId: String,
    val simulationId: String,
    val config: SimulationConfig
)

package handler

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.ktor.websocket.*
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.coroutineScope
import model.LaunchSimulationRequest
import pipeline.Event
import simulation.*

class LaunchSimulationHandler {

    private val simulationMap = mapOf(
        "1" to SimpleLoopSimulation(),
        "2" to SequentialFlowSimulation(),
        "3" to SequentialFlowWithBufferingSimulation(),
        "4" to ConcurrentFlowSimulation(),
        "5" to ConcurrentFlowWithBufferingSimulation()
    )

    suspend fun launchSimulation(request: LaunchSimulationRequest, outgoing: SendChannel<Frame>) {
        coroutineScope {
            val eventChannel = Channel<Event<Any>>()

            async {
                eventChannel.copyTo(outgoing)
            }

            val simulation = simulationMap[request.simulationId]
            simulation?.launch(request.config, eventChannel)
        }
    }

}

suspend fun Channel<Event<Any>>.copyTo(outgoing: SendChannel<Frame>) {
    val mapper = jacksonObjectMapper()
    mapper.registerModule(JavaTimeModule())

    this.consumeEach { event ->
        outgoing.send(Frame.Text(mapper.writeValueAsString(event)))
    }
}


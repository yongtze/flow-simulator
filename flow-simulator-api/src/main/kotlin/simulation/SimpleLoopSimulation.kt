package simulation

import kotlinx.coroutines.channels.SendChannel
import model.Item
import model.Page
import model.SimulationConfig
import pipeline.*
import simulation.item.ItemGenerator
import simulation.item.processItem

class SimpleLoopSimulation: FlowSimulation {

    private val itemGenerator = ItemGenerator();

    suspend fun plainLoop(config: SimulationConfig, eventChannel: SendChannel<Event<Any>>) {
        var pageIndex: Int = 0
        var page: Page<Item>?

        do {
            page = itemGenerator.generatePage(config.fetchConfig.totalItems, config.fetchConfig.itemsPerPage, pageIndex)
            pageIndex++

            for (item in page.items) {
                processItem(item, config.processConfig)
            }
        } while (page?.hasNext == true)
    }

    override suspend fun launch(config: SimulationConfig, eventChannel: SendChannel<Event<Any>>) {
        var pageIndex: Int = 0
        var page: Page<Item>?

        eventChannel.simulationStarted()
        do {
            eventChannel.enteredFetching(Page<Item>(index = pageIndex))

            page = itemGenerator.generatePage(config.fetchConfig.totalItems, config.fetchConfig.itemsPerPage, pageIndex)
            pageIndex++

            eventChannel.exitedFetching(page)
            eventChannel.enteredUnpacking(page)
            for (item in page.items) {
                eventChannel.exitedUnpacking(item)
                eventChannel.enteredProcessing(item)

                processItem(item, config.processConfig)

                eventChannel.exitedProcessing(item)
                eventChannel.enteredDone(item)
            }
            eventChannel.exitedUnpacking(page.copy(items = listOf()))
        } while (page?.hasNext == true)
        eventChannel.simulationEnded()
    }

}
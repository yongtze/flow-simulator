package simulation.item

import kotlinx.coroutines.delay
import model.Item
import model.ProcessConfig
import kotlin.random.Random
import kotlin.random.nextLong

suspend fun processItem(item: Item, processConfig: ProcessConfig) {
    delay(Random.nextLong(processConfig.processTimeRange))
}

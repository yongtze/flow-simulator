package simulation.item

import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import model.Item
import model.Page
import pipeline.*
import kotlin.random.Random
import kotlin.random.nextLong

class ItemGenerator {

    fun generatePagesFlow(totalItems: Int, itemsPerPage: Int, eventChannel: SendChannel<Event<Any>>, fetchTimeRange: LongRange = (500L .. 1000L)) = flow {
        var page: Page<Item>? = null
        var pageIndex: Int = 0
        while (page == null || page.hasNext) {
            eventChannel.enteredFetching(Page<Item>(index = pageIndex, hasNext = true))

            page = generatePage(totalItems, itemsPerPage, pageIndex, fetchTimeRange)
            pageIndex++
            emit(page)

            eventChannel.exitedFetching(page)
        }
    }

    suspend fun generatePage(totalItems: Int, itemsPerPage: Int, pageIndex: Int = 0, fetchTimeRange: LongRange = (500L .. 1000L)): Page<Item> {
        delay(Random.nextLong(fetchTimeRange))

        val offset = pageIndex * itemsPerPage
        val totalPages = (totalItems + itemsPerPage - 1) / itemsPerPage
        val itemCountInPage = if (pageIndex == totalPages - 1) (((totalItems-1) % itemsPerPage) + 1) else itemsPerPage
        val hasNext = pageIndex < totalPages - 1

        return Page(
            index = pageIndex,
            items = generateItems(itemCountInPage, offset),
            hasNext = hasNext
        )
    }

    private fun generateItems(count: Int, offset: Int = 0): List<Item> {
        return (0 until count).map { i -> Item(i + offset) }.toList()
    }

}
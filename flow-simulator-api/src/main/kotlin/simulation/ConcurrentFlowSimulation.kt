package simulation

import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.flow.*
import model.SimulationConfig
import pipeline.*
import simulation.item.ItemGenerator
import simulation.item.processItem

@OptIn(FlowPreview::class)
class ConcurrentFlowSimulation : FlowSimulation {

    private val itemGenerator = ItemGenerator()

    suspend fun plainFlow(config: SimulationConfig, eventChannel: SendChannel<Event<Any>>) {
        val pageFlow = itemGenerator.generatePagesFlow(
            totalItems = config.fetchConfig.totalItems,
            itemsPerPage = config.fetchConfig.itemsPerPage,
            eventChannel = eventChannel,
            fetchTimeRange = config.fetchConfig.fetchTimeRange
        )

        pageFlow
            .flatMapConcat { page -> page.items.asFlow() }
            .flatMapMerge(config.processConfig.concurrency) { item ->
                flowOf(item)
                    .onEach { item -> processItem(item, config.processConfig) }
            }
            .collect()
    }

    override suspend fun launch(config: SimulationConfig, eventChannel: SendChannel<Event<Any>>) {
        val pageFlow = itemGenerator.generatePagesFlow(
            totalItems = config.fetchConfig.totalItems,
            itemsPerPage = config.fetchConfig.itemsPerPage,
            eventChannel = eventChannel,
            fetchTimeRange = config.fetchConfig.fetchTimeRange
        )

        pageFlow
            .onStart { eventChannel.simulationStarted() }
            .flatMapConcat { page ->
                page.items.asFlow()
                    .onStart { eventChannel.enteredUnpacking(page) }
                    .onEach { item -> eventChannel.exitedUnpacking(item) }
                    .onCompletion { eventChannel.exitedUnpacking(page) }

            }
            .flatMapMerge(config.processConfig.concurrency) { item ->
                flowOf(item)
                    .onEach { item ->
                        eventChannel.enteredProcessing(item)
                        processItem(item, config.processConfig)
                        eventChannel.exitedProcessing(item)
                    }
            }
            .onEach { item -> eventChannel.enteredDone(item) }
            .onCompletion { eventChannel.simulationEnded() }
            .collect()
    }

}
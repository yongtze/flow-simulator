package simulation

import kotlinx.coroutines.channels.SendChannel
import model.SimulationConfig
import pipeline.Event

sealed interface FlowSimulation {
    suspend fun launch(config: SimulationConfig, eventChannel: SendChannel<Event<Any>>)
}
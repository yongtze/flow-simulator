import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import handler.LaunchSimulationHandler
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.http.content.*
import io.ktor.server.netty.*
import io.ktor.server.routing.*
import io.ktor.server.websocket.*
import io.ktor.websocket.*
import model.LaunchSimulationRequest
import java.io.File


fun Application.main() {
    routing {
        routes()
    }
}

fun Routing.routes() {
    val mapper = jacksonObjectMapper()
    mapper.registerModule(JavaTimeModule())

    val handler = LaunchSimulationHandler()

    static("/") {
        staticRootFolder = File("static")
        default("index.html")
    }

    webSocket("/simulation/launch") {
        for (frame in incoming) {
            frame as? Frame.Text ?: continue
            val request = mapper.readValue(frame.readText(), LaunchSimulationRequest::class.java)
            println(request)
            handler.launchSimulation(request, outgoing)
        }
    }
}

fun main(args: Array<String>) {
    embeddedServer(Netty, port = 8080) {
        install(WebSockets)
        main()
    }.start(wait = true)
}
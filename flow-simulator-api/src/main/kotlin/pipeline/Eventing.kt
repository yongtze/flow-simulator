package pipeline

import kotlinx.coroutines.channels.SendChannel
import java.time.Instant
import java.util.*

enum class EventType {
    SimulationStarted, EnteredStage, ExitedStage, ExitedStageWithError, SimulationEnded
}

enum class EventStage {
    Fetching, Unpacking, Buffering, Processing, Done
}

data class Event<T>(
    val eventId: UUID = UUID.randomUUID(),
    val eventType: EventType,
    val eventStage: EventStage? = null,
    val eventTs: Instant = Instant.now(),
    val data: T? = null,
    val errorMessage: String? = null
)

suspend fun SendChannel<Event<Any>>.simulationStarted() {
    this.send(Event(eventType = EventType.SimulationStarted))
}

suspend fun SendChannel<Event<Any>>.simulationEnded() {
    this.send(Event(eventType = EventType.SimulationEnded))
}

suspend fun SendChannel<Event<Any>>.enteredStage(eventStage: EventStage, data: Any) {
    this.send(
        Event(
            eventType = EventType.EnteredStage,
            eventStage = eventStage,
            data = data,
        )
    )
}

suspend fun SendChannel<Event<Any>>.exitedStage(eventStage: EventStage, data: Any) {
    this.send(
        Event(
            eventType = EventType.ExitedStage,
            eventStage = eventStage,
            data = data
        )
    )
}

suspend fun SendChannel<Event<Any>>.enteredFetching(data: Any) {
    this.enteredStage(EventStage.Fetching, data)
}

suspend fun SendChannel<Event<Any>>.exitedFetching(data: Any) {
    this.exitedStage(EventStage.Fetching, data)
}

suspend fun SendChannel<Event<Any>>.enteredUnpacking(data: Any) {
    this.enteredStage(EventStage.Unpacking, data)
}

suspend fun SendChannel<Event<Any>>.exitedUnpacking(data: Any) {
    this.exitedStage(EventStage.Unpacking, data)
}

suspend fun SendChannel<Event<Any>>.enteredBuffering(data: Any) {
    this.enteredStage(EventStage.Buffering, data)
}

suspend fun SendChannel<Event<Any>>.exitedBuffering(data: Any) {
    this.exitedStage(EventStage.Buffering, data)
}

suspend fun SendChannel<Event<Any>>.enteredProcessing(data: Any) {
    this.enteredStage(EventStage.Processing, data)
}

suspend fun SendChannel<Event<Any>>.exitedProcessing(data: Any) {
    this.exitedStage(EventStage.Processing, data)
}

suspend fun SendChannel<Event<Any>>.enteredDone(data: Any) {
    this.enteredStage(EventStage.Done, data)
}

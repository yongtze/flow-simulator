
import type { Config } from './Config';


export interface RunSimulationRequest {
    requestId: string;
    simulationId: string;
    config: Config;
}


export interface Range {
    start: number;
    end: number;
}

export interface FetchConfig {
    totalItems: number,
    itemsPerPage: number,
    fetchTimeRange: Range;
}


export interface ProcessConfig {
    concurrency: number;
    processTimeRange: Range;
}

export interface Config {

    fetchConfig: FetchConfig;

    bufferSize: number;

    processConfig: ProcessConfig;

}



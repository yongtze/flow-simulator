import type { BoardEntity, EntityType } from "./BoardEntity";

export class Stage {
    stageName: string;
    entityType: EntityType;
    entities: BoardEntity[];
}


export type EntityType = "Page" | "Item";

export interface Item {
    entityId: string;
    entityType: "Item";
}

export interface Page {
    entityId: string;
    entityType: "Page";
    items: Item[];
}

export type BoardEntity = Page | Item;

export interface Stage {
    stageName: string;
    entityType: EntityType;
    isStart: boolean;
    isTerminal: boolean;
    isEntityTypeTerminal: boolean;
}

export interface Simulation {
    simulationId: string;
    simulationName: string;
    stages: Stage[];
}
